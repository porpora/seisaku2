require './timer.rb'
require './clock.rb'
require './stopwatch.rb'
require 'bigdecimal'


class Top
  def clear_stdout
    puts "\e[H\e[2J"
  end

  def run
    count = 3
    loop do
      while 0 < count
        clear_stdout
        puts "\n\s起動中です #{count}"
        sleep(1)
        count -= 1
      end
      break
    end

    loop do
      clear_stdout
      print "
      1.時計
      2.タイマー
      3.ストップウォッチ
      9.アプリを終了する。
      "

      num = gets.chomp
      case num
      when '1'
        clock = Clock.new
        clock.timer
      when '2'
        timer = Timer.new
        timer.run
      when '3'
        stop_game = StopGame.new
        stop_game.run
      when '9'
        break
      end # case
    end # loop do
  end # def
end # class

top = Top.new
top.run
