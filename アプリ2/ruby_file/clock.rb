require"date"
require"time"

class Time #タイムゾーンを取得するクラス
  #タイムゾーン取得
  def timezone(timezone = 'UTC')
    old = ENV['TZ']
    utc = self.dup.utc
    ENV['TZ'] = timezone
    output = utc.localtime
    ENV['TZ'] = old
    output
  end # def timezone
end # class Time

class Clock #メニューのクラス
  #initializeメゾット
  def initialize
    @format_for_time = "%Y/%m/%d %H:%M:%S (%A) %Z"
    @timezone = 'Asia/Tokyo'
  end # def initialize

  def clear_stdout
    puts "\e[H\e[2J"
  end

  #アラーム
  def alarm
    puts "\e[H\e[2J"
    puts ""
    puts "時刻を入力して下さい"
    hour = gets.chomp.to_i
    min = gets.chomp.to_i
    puts "アラーム セット完了"
    now = Time.now.timezone(@timezone)+32400 #日本標準時に固定
    year = now.year
    mon = now.mon
    day = now.day
    alarm_time = Time.local(year, mon, day, hour, min)
    x = alarm_time - now
    p alarm_time
    sleep x
    puts "時間です！"
    sleep(3)
    puts "アラーム終了"
    sleep(1)
    puts "\e[H\e[2J"
  end #def alarm

  #タイムゾーン変更
  def country
    puts "\e[H\e[2J"
    puts "表示したい都市の番号を入力してください"
    puts ""
    puts "1.東京"
    puts "2.ニューヨーク"
    puts "3.ロンドン"
    puts "4.パリ"
    puts "5.シドニー"
    puts "6.香港"
    puts ""

    time = gets.to_i
    puts""
    puts "\e[H\e[2J"

    if time == 1 then #東京
      @timezone = 'Asia/Tokyo'

   elsif time == 2 then #ニューヨーク
      @timezone = 'America/New_York'

   elsif time == 3 then #ロンドン
     @timezone = "Europe/London"

   elsif time == 4 then #パリ
     @timezone =  "Europe/Paris"

   elsif time == 5 then #シドニー
     @timezone =  'Australia/Sydne'

   elsif time == 6 then #香港
     @timezone = "Asia/Hong_Kong"

    else
    end #if
  end #def timezone

  #時間表記変更
  def chenge
    puts "\e[H\e[2J"
    puts ""
    puts"表示したい時刻の番号を入力してください"
    puts ""
    puts"1. 24時間 表示"
    puts ""
    puts"2. AM・PM 表示"
    puts ""
    time = gets.to_i
    puts""
    puts "\e[H\e[2J"
    if time == 1 then
      @format_for_time = "%Y/%m/%d %H:%M:%S (%A) %Z"
    elsif time == 2 then
      @format_for_time = "%Y/%m/%d %p %I:%M:%S (%A) %Z"
    else
    end #if
  end #def change

  #時計のメゾット
  def timer
    thread = Thread.new do
      while true
        loopflag = true
        Time == 24*60*60
        Signal.trap('INT') do
          # 捕捉した場合シャットダウンする。
          loopflag = false
        end #Signal.trap('INT') do

        while(loopflag) do
          print "\r"
          print Time.now.timezone(@timezone).strftime( @format_for_time )
          print "\r"
          STDOUT.flush
          sleep(1);
        end #while (loopflag) do
      end #while true
    end #thread = Thread.new do

    #メニュー
    while true
      clear_stdout
      print "
      1.時間表記表示変更
      2.タイムゾーン変更
      3.アラーム
      9.終了"
      puts""
      puts""
      num = gets.chomp
      case
      when '1' == num
        chenge
        #時刻表記変更
      when '2' == num
        country
        #タイムゾーン変更
      when '3' == num
        alarm
        #アラーム設定
      when '9' == num
        #アプリ終了
    Thread::kill(thread)
        break
      else
      end #case
    end #while true
  end #def timer
end #class Clock
