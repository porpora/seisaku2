class StopGame

  def clear
     puts "\e[H\e[2J"
  end # def clear

  def menu
     @ranking = []
     @back_ranking = []
      loop do
     puts "1: 2人で遊ぶ"
     puts "2: 3人で遊ぶ"
     puts "9: 終了"
     @input = gets.chomp
        case @input
          when "1"
            clear
            puts "1プレイヤーの名前を入力してください。"
            @pleyer1 = gets.chomp
            puts "2プレイヤーの名前を入力してください。"
            @pleyer2 = gets.chomp
            clear

            puts "#{@pleyer1}の番です"
            timebattle
            puts "#{@pleyer2}の番です"
            timebattle
            ranking_2p
            break

          when "2"
            clear
            puts "1プレイヤーの名前を入力してください。"
            @pleyer1 = gets.chomp
            puts "2プレイヤーの名前を入力してください。"
            @pleyer2 = gets.chomp
            puts "3プレイヤーの名前を入力してください。"
            @pleyer3 = gets.chomp
            clear
            puts "#{@pleyer1}の番です"
            timebattle
            puts "#{@pleyer2}の番です"
            timebattle
            puts "#{@pleyer3}の番です"
            timebattle
            ranking_3p
            break
          when "9"
            break
          else
            puts "正しい値を入力してください"
            next
        end #case
      end #loop do
    end #def menu

  def timebattle
    @score = []
    count = 0
    # 処理を3回繰り返す
    3.times do
      puts "エンターで計測開始"
      gets
      clear
      start = Time.now
      3.times do
          puts "Start!!"
          p count
          sleep (0.95)
          count += 1
          clear
      end
    puts "エンターで入力停止"
    gets
    clear
    count = 0
    stop = Time.now
    # 計算結果を格納し、配列の末尾に代
    stop_time = stop - start
    @score.push(stop_time)
    # igarashi = @score[0]
    # miyauti = @score [1]
    end # 繰り返しの終了
    calculation
  end #timebattle


  # 計算を行うメソッド
  def calculation
  # 各値の小数点第二位以下を切り捨て
    @score3 = []
    @score3 = BigDecimal(@score[0].to_s).floor(2).to_f,
              BigDecimal(@score[1].to_s).floor(2).to_f,
              BigDecimal(@score[2].to_s).floor(2).to_f

  # 1,2,3回目の計測秒数表示
              puts "1回目#{@score3[0]}秒"
              puts "2回目#{@score3[1]}秒"
              puts "3回目#{@score3[2]}秒"

  # 計測値の和
    default_score = @score3[0] + @score3[1] + @score3[2]
    puts "合計#{default_score}秒です。"

  #小数点第二位以下を切り捨て、30秒との差異を求める
  #stringにしないと切り捨てできない。
    total_score = BigDecimal(default_score.to_s).floor(2).to_f
    @defference_sec = total_score - 30

      if @defference_sec < 0
         @defference_sec * -1
      end #if

    @back_ranking.push(@defference_sec)
    @ranking.push(total_score)
  end # メソッド



  def ranking_2p
     player1 = "#{@pleyer1} #{@back_ranking[0]}秒"
     player2 = "#{@pleyer2} #{@back_ranking[1]}秒"
       if @back_ranking[0] < @back_ranking[1]
         puts "1位 #{player1}"
         puts "2位 #{player2}"
       else
         puts "1位 #{player2}"
         puts "2位 #{player1}"
       end #if
     #結果を初期化する
     @ranking = []
     @back_ranking = []
  end #def ranking_2p



  def ranking_3p
       player1 = "#{@pleyer1} #{@back_ranking[0]}"
       player2 = "#{@pleyer2} #{@back_ranking[1]}"
       player3 = "#{@pleyer3} #{@back_ranking[2]}" # @backranking = 計測した値と30秒との差異
  case
  when @back_ranking[0] <= @back_ranking[1] && @back_ranking[0] <= @back_ranking[2] && @back_ranking[1] <= @back_ranking[2]
    puts "1位： #{player1}秒"
    puts "2位： #{player2}秒"
    puts "3位： #{player3}秒"

  when @back_ranking[0] <= @back_ranking[1] && @back_ranking[0] <= @back_ranking[2] && @back_ranking[2] <= @back_ranking[1]
    puts "1位： #{player1}秒"
    puts "2位： #{player3}秒"
    puts "3位： #{player2}秒"

  when @back_ranking[1] <= @back_ranking[0] && @back_ranking[1] <= @back_ranking[2] && @back_ranking[0] <= @back_ranking[2]
    puts "1位： #{player2}秒"
    puts "2位： #{player1}秒"
    puts "3位： #{player3}秒"

  when @back_ranking[1] <= @back_ranking[0] && @back_ranking[1] <= @back_ranking[2] && @back_ranking[2] <= @back_ranking[0]
    puts "1位： #{player2}秒"
    puts "2位： #{player3}秒"
    puts "3位： #{player1}秒"

  when @back_ranking[2] <= @back_ranking[1] && @back_ranking[2] <= @back_ranking[0] && @back_ranking[0] <= @back_ranking[1]
    puts "1位： #{player3}秒"
    puts "2位： #{player1}秒"
    puts "3位： #{player2}秒"

  when @back_ranking[2] <= @back_ranking[1] && @back_ranking[2] <= @back_ranking[0] && @back_ranking[1] <= @back_ranking[0]
    puts "1位： #{player3}秒"
    puts "2位： #{player2}秒"
    puts "3位： #{player1}秒"
  end #case
  #結果を初期化する
  @ranking = []
  @back_ranking = []
  end #def ranking_3p










  def watch
    @sec = 0
    @usec = 0
    @nsec = 0

    puts "エンターで計測を開始します。"
    gets
    restart
  end # def watch

  def restart
    thread2 = Thread.fork do
      loop do
        clear
        puts "#{@sec}:#{@usec}#{@nsec}"
        sleep (0.01)
        @nsec += 1
        carry
      end # loop
    end # thread
    if gets
      Thread::kill(thread2)
      escape
    end # if gets
  end # def restart

  def restart_rap
    thread2 = Thread.fork do
      loop do
        clear
        puts "#{@rap}:#{@urap}#{@nrap}"
        #puts @urap
        #puts @nrap
        puts "#{@sec}:#{@usec}#{@nsec}"
        sleep (0.01)
        @nsec += 1
        carry
      end # loop
    end # thread
    if gets
      Thread::kill(thread2)
      escape
    end # if gets
  end # def restart

  def escape
    loop do
    puts "1:計測を再開します。"
    puts "2:計測値をリセットします"
    puts "3:RAP"
    puts "5:終了します。"
    @input = gets.chomp
      case @input
        when "1"
          restart
          break
        when "2"
          @sec = 0
          @usec = 0
          @nsec = 0
          clear
        when "3"
          #@rap = []
          #@rap.push(@sec, @usec, @nsec)
          #print "#{@rap[0]}:#{@rap[1]}#{@nsec[2]}"
          @rap = @sec
          @urap = @usec
          @nrap = @nsec
          restart_rap
          break
        when "5"
          puts "アプリを終了します"
          break
        else
          puts "正しい値を入力してください"
        #next
       end # case
     end # loop
  end #def escape

  def carry
    case
      when @nsec == 10
             @usec += 1
             @nsec = 0

      when @usec == 10
             @sec += 1
             @usec = 0
    end # case
  end # def carry



  def run
    clear
    loop do
      puts"
      1: ストップウォッチ
      2: ストップゲーム
      9: 終了
       "
      @input = gets.chomp
      case @input
        when "1"
          watch
        when "2"
          menu
        when "9"
          break
        else
          puts "正しい値を入力してください。"
      end # case
    end # loop
  end # def run
end # class
