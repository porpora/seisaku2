class Timer
  def initialize
    @input_name = "※新規作成してください。"
    @user_input_hours = ''
    @user_input_minutes = ''
    @user_input_seconds = ''
  end

  def clear_stdout
    puts "\e[H\e[2J"
  end

  def threemin
    puts "\n\s\sスタートする場合はEnterを押してください"
    puts "\s\s何かキーを押下した後Enterを押した場合はメニューに戻ります。"
    if gets.chomp.empty?
      stop_flag = false
      isBreak = false
      threemin_thread = Thread.new do
        m = 2
        while 0 <= m
          s = 59
          while 0 <= s
            clear_stdout
            min = sprintf("%02d" , m)
            sec = sprintf("%02d" , s)
            puts "\s\s\s#{min}:#{sec}"
            puts "\n\n\n\s・Enter押下にて一時停止します。"
            sleep (1)
            s -= 1
            if stop_flag
              Thread.stop
            end # stop_flag
          end # while 0 <= s
          m -= 1
        end # while 0 <= m
        puts "3分経過しました。 Enterキーを押してください。"
        isBreak = true
      end # thread
      loop do
        gets.chomp.empty?
        break if isBreak
        if stop_flag == false
          stop_flag = true
          print "\n
    1.カウントを再開する。
    2.メニューに戻る。\n"
        num = gets.chomp
          case num
          when '1'
            stop_flag = false
            threemin_thread.run
          when '2'
            break
          end # case
        end # if stop_flag == false
        stop_flag = false
        puts "入力値がありませんEnterを押してください。"
      end # loop
    end # if empty
  end # def

# 秒数を設定するメソッドです。
# only_second 真偽値 true:hour,minuteが0のとき
  def set_seconds( only_seconds = false )
    before_message = only_seconds ? "\n\s「秒」を入力してください(1～60sec):" : "\n\s「秒」を入力してください(0～59sec):"
    puts before_message
    @seconds_num = gets.chomp.to_i
  end # def end

  def setting
    clear_stdout
    puts " ※適切な値でない場合は全て「0」が入力されます。"
    puts "\n\s「時」を入力してください(0～24h)："
    @hours_num = gets.chomp.to_i
    if @hours_num == 24
      @minutes_num = 0
      @seconds_num = 0 # 24:00:00
    elsif @hours_num.between?(1, 23) # 1~23h ??min ??sec
      puts "\n\s「分」を入力してください(0～59min)："
      @minutes_num = gets.chomp.to_i
      if @minutes_num.between?(0, 59) # 1~23h 0~59min ??sec
        set_seconds
      else # minutes
        @minutes_num = 0 # 1~23h 0min ??sec
        set_seconds
      end # if minutes_num.between?(0, 59)
    else # hours
      @hours_num = 0
      puts "\n\s「分」を入力してください(0～60min)："
      @minutes_num = gets.chomp.to_i
      if @minutes_num == 60 # 0h 60min ??sec
        @seconds_num = 0 # 0h 60min 0sec
      elsif @minutes_num.between?(1, 59) # 0h 1~59min ??sec
        set_seconds
      else  # 0h 0min ?sec
        @minutes_num = 0
        set_seconds(true)
      end # if minutes_num == 60
    end # if hour
  end # setting

  def start
    if (@hours_num.to_s =~ /^[0-9]+$/ && @minutes_num.to_s =~ /^[0-9]+$/ && @seconds_num.to_s =~ /^[0-9]+$/) == 0
      if @hours_num == 0 && @minutes_num == 0 && @seconds_num == 0
        puts " 00：00：00は指定できません。"
      else
        puts "\n\s\sスタートする場合はEnterを押してください"
        puts "\s\s何かキーを押下した後Enterを押した場合はメニューに戻ります。"
        if gets.chomp.empty?
          stop_flag = false
          isBreak = false
          start_thread = Thread.new do
            hours = @hours_num
            minutes = @minutes_num
            seconds = @seconds_num
            while 0 <= hours
              while 0 <= minutes
                while 0 <= seconds
                  clear_stdout
                  hour = sprintf("%02d" , hours)
                  minute = sprintf("%02d" , minutes)
                  second = sprintf("%02d" , seconds)
                  puts "\s\s\s#{hour}:#{minute}:#{second}"
                  puts "\n\n\n\s・Enter押下にて一時停止します。"
                  sleep (1)
                  seconds -= 1
                  if stop_flag
                    Thread.stop
                  end # stop_flag
                end # sec while
                seconds = 59
                minutes -= 1
              end # min while
              minutes = 59
              hours -= 1
            end # hour while
            puts "\n\s時間が計測したためEnterキーを押してください。"
            isBreak = true
          end # do end
          loop do
            gets.chomp.empty?
            break if isBreak
            if stop_flag == false
              stop_flag = true
              print "\n
    1.カウントを再開する。
    2.メニューに戻る。"
              num = gets.chomp
              case num
              when '1'
                stop_flag = false
                start_thread.run
              when '2'
                break
              end # case
            end # if stop_flag == false
            stop_flag = false
            puts "入力値がありませんEnterを押してください。"
          end # loop
        end # if empty
      end # if &&
    end # if //
    end # def start

  def interval
    clear_stdout
    puts "\s※適切な値でない場合は全て「0」が入力されます。"
    puts "\n\s「分」を入力してください(0～60min)："
    @minutes_num = gets.chomp.to_i
    if @minutes_num.between?(0, 59) # 0~59min ??sec
      set_seconds
    elsif @minutes_num == 60
      second = 0
    else @minutes_num == 0
      set_seconds(true)
    end # if minute

    if @minutes_num == 0 && @seconds_num == 0
      puts "\s00：00は指定できません。"
    else

    puts "\n\sこの値で設定しました。"
    minute = sprintf("%02d" , @minutes_num)
    second = sprintf("%02d" , @seconds_num)
    puts "#{minute}:#{second}"

    puts "\n\s・時間経過後の休憩時間を指定してください(秒)"
    @sleep_num = gets.chomp.to_i
      puts "\n\sスタートする場合はEnterを押してください"
      puts "\s何かキーを押下した後Enterを押した場合はメニューに戻ります。"
      if gets.chomp.empty?
        stop_flag = false
        interval_thread = Thread.new do
          loop do
            minutes = @minutes_num
            seconds = @seconds_num
            while 0 <= minutes
              while 0 <= seconds
                clear_stdout
                minute = sprintf("%02d" , minutes)
                second = sprintf("%02d" , seconds)
                puts "\s\s\s#{minute}:#{second}"
                puts "\n\n\n\s\s・Enter押下にて一時停止します。"
                sleep (1)
                seconds -= 1
                if stop_flag
                  Thread.stop
                end # stop_flag
              end # sec while
              seconds = 59
              minutes -= 1
            end # min while
            sleep_num = @sleep_num
            while 0 <= sleep_num
              clear_stdout
              puts "\s時間が経過しましたので#{sleep_num}秒休憩します。"
              puts "\n\n\n\s押下にて一時停止します。"
              sleep(1)
              sleep_num -= 1
            end # sleep while
          end # loop
        end # do
        loop do
          gets.chomp.empty?
          if stop_flag == false
            stop_flag = true
            print "\n
    1.カウントを再開する。
    2.メニューに戻る。"
            num = gets.chomp
            case num
            when '1'
              stop_flag = false
              interval_thread.run
            when '2'
              break
            end
          end # if stop_flag == false
        end # loop
        stop_flag = false
        puts "入力値がありませんEnterを押してください。"
      end # if empty
    end # if &&
  end

  def confirmation
    hour = sprintf("%02d" , @user_input_hours)
    minute = sprintf("%02d" , @user_input_minutes)
    second = sprintf("%02d" , @user_input_seconds)
    return "#{hour}:#{minute}:#{second}"
  end

  def new_timer
    setting
    @user_input_hours = @hours_num
    @user_input_minutes = @minutes_num
    @user_input_seconds = @seconds_num
    clear_stdout
    puts "\sこちらの値で設定してよろしいですか？「#{confirmation}」"
    puts "\n\sよろしければEnterを押してください"
    if gets.chomp.empty?
      clear_stdout
      puts "\s設定したカウントの名前を入力してください:"
      @input_name = gets.chomp
      puts "\n\n\s\s～～入力した値を保存しています～～"
      sleep(3)
    else
      initialize
    end
  end

  def user_timer_start
    clear_stdout
    if (@user_input_hours.to_s =~ /^[0-9]+$/ && @user_input_minutes.to_s =~ /^[0-9]+$/ && @user_input_seconds.to_s =~ /^[0-9]+$/) == 0
      puts "\s\s設定した値はこちらです。「#{confirmation}」"
      @hours_num = @user_input_hours
      @minutes_num = @user_input_minutes
      @seconds_num = @user_input_seconds
      start
    end
  end

  def delete_timer
    if (@user_input_hours.to_s =~ /^[0-9]+$/ && @user_input_minutes.to_s =~ /^[0-9]+$/ && @user_input_seconds.to_s =~ /^[0-9]+$/) == 0
      puts "#{@input_name}:「#{confirmation}」"
      puts "こちらの内容を消す場合は「delete」と入力してください。"
      user_input = gets.chomp
      if user_input == "delete"
        initialize
      end
    end
  end

  def run
    loop do
      clear_stdout
      puts "
      1.3分タイマー
      2.タイマー(時間指定)
      3.インターバルタイマー
      4.#{@input_name}
      5.新規作成
      6.削除(対象:4)
      9.戻る"

      num = gets.chomp
      case
      when '1' == num
        threemin
      when '2' == num
        setting
        start
      when '3' == num
        interval
      when '4' == num
        user_timer_start
      when '5' == num
        new_timer
      when '6' == num
        delete_timer
      when '9' == num
        break
      end # case
    end # loop
  end # def run
end # class Timer

timer = Timer.new
timer.run
