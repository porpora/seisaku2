class LastBattle
  def initialize( dbh )
    @dbh = dbh
  end

  #ボスのHPを初期化する
  def boss_hpreset
    hp_reset = @dbh.execute(
     "update enemy set HP = (select MAX_HP from enemy where id = 2) where id = 2;")
  end #def boss_hpreset

  #プレイヤーの攻撃処理
  #キャラクターの攻撃力を取り出す
  def character_atack
    character_power = @dbh.execute(
      "select POWER from character where id = 1;")
    character_power.each do |val|
      @character_power = val
    end #character_power

  #エネミーのHPを取り出す
    enemy_hp = @dbh.execute(
      "select HP from enemy where id = 2;")
    enemy_hp.each do |val|
      @enemy_hp = val
      #puts "敵の残りHPは#{@enemy_hp[0]}"
    end # enemy_hp

  #エネミーの防御力を取り出す
    enemy_defense = @dbh.execute(
      "select DEFENSE from enemy where id = 2;")
    enemy_defense.each do |val|
      @enemy_defense = val
    end #enemy_defense

  #キャラクターの攻撃力とエネミーの防御力を引き算し、ダメージ値を作る

    @damege = @character_power[0] - @enemy_defense[0]
    p "#{@damege} のダメージを与えた！"

  #敵のHPからダメージ値を引き算する
      enemy_hp_record = @dbh.execute(
        "UPDATE enemy SET HP = #{@enemy_hp[0] - @damege}")
  end # def character_atack



  #エネミーの攻撃処理
  def enemy_atack
  #ボスの攻撃力を取り出す
      enemy_power = @dbh.execute(
        "select POWER from enemy where id = 2;")
      enemy_power.each do |val|
        @enemy_power = val
      end

  #キャラクターのHPを取り出す
      character_hp = @dbh.execute(
         "select HP from character where id = 1;")
      character_hp.each do |val|
          @character_hp = val
      end

  #キャラクターの防御力を取り出す
      character_defense = @dbh.execute(
          "select DEFENSE from character where id = 1;")
      character_defense.each do |val|
          @character_defense = val
      end

  #ボスの攻撃力とキャラクターの防御力を引き算し、ダメージ値を作る
      @defense_damege = @enemy_power[0] - @character_defense[0]
        if @defense_damege < 0
           p "0ダメージを受けた！"
              else
           p "#{@defense_damege} のダメージを受けた"

        end

  #キャラクターのHPからダメージ値を引き算する
        character_hp_record = @dbh.execute(
            "UPDATE character SET HP = #{@character_hp[0] - @defense_damege}")
  end # def enemy_atack



  #キャラクターの経験値とお金の値を取り出す
  def exp_and_money
    oppai =  @dbh.execute(
        "select EXP_POINT, MONEY from character where id = 1;")
    oppai.each do |val|
        @character_exp_and_maney = val
        p "#{@character_exp_and_maney}をかくとく！"
    end

  #エネミーの経験値とお金の値を取り出す
    enemy_exp_and_money = @dbh.execute(
        "select EXPERIENCE_POINT, MONEY from enemy where id = 2;")
    enemy_exp_and_money.each do |val|
        @enemy_exp_and_money = val
        p "#{@character_exp_and_maney}をかくとく!"
    end

  #キャラクターの経験値・マネーと、エネミーの経験値・マネーを合算する
     total_exp = @dbh.execute(
          "UPDATE character SET EXP_POINT = #{@enemy_exp_and_money[0] + @character_exp_and_maney[0]},
           MONEY = #{@enemy_exp_and_money[1] + @character_exp_and_maney[1]}")
  end # def exp_and_money

  def run
    puts "\n  鈴木が現れた！"
     boss_hpreset
    loop do
      puts "  1:こうげき"
      puts "  2:にげる"

      @input = gets.chomp
      case @input
      when "1"
        character_atack
        enemy_atack
  #勝利した場合の条件分岐
      if @enemy_hp[0] < 0
        exp_and_money
        happyend = Happyend.new(@dbh)
        happyend.run
        break
  #敗北した場合の条件分岐
      elsif
          badend = Badend.new
          badend.run
          break
      end # if

      when "2"
        puts "  逃れることはできない。"
      end #loop do
    end #case
  end # def run
end
