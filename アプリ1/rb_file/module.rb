module CharacterDisplay
   def date_load
      item_name = {'id' => "ID", 'USER_NAME' => "名前", 'SEX' => "性別", 'POWER' => "攻撃力", 'DEFENSE' => "防御力", 'LEVEL' => "レベル", 'MONEY' => "所持金", 'HP' => "体力", 'EXP_POINT' => "経験値", 'MAX_EXP_POINT' => "目標経験値", 'MAX_HP' => "最大HP" }
      sth = @dbh.execute(
      "select * from character where id = 1;"
      )
      columns = sth.schema.columns.to_a
      character_info = sth.first

      col_num = 0
      puts "-----------------------------------------"
      character_info.each do |val|
        puts "#{item_name[columns[col_num].name.to_s]}: #{val.to_s}"
        col_num += 1
      end # do
      puts "-----------------------------------------"
   end
end
