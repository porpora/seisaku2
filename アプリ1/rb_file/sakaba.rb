#酒場のクラス
class Sakaba
  #マスターと会話・冒険について
  def initialize( dbh )
    @dbh = dbh
  end

  def mastertalk1
    sleep(0.9)
    puts "\n#{$user_name}:どこに行けばいいかな？"
    puts""
    sleep(0.9)
    puts "\nマスター：それなら村を出て西にある神殿だな"
    puts""
    sleep(0.9)
    puts "\n#{$user_name}:神殿？"
    puts""
    sleep(0.9)
    puts "\nマスター：そう。噂じゃ神殿の一番奥に魔王がいるって噂だ"
    sleep(0.9)
    puts "\n          まあ誰も見たことないしただの噂だといいけどな"
    puts""
    sleep(0.9)
    puts "\n#{$user_name}:西の神殿か・・・行ってみるか"
    puts""
    sleep(0.9)
    puts "\nマスター：あんた行くのかい？気を付けな。武器ならそこの剣士が詳しいぜ"
    puts""
    sleep(0.9)
    puts "\n#{$user_name}:ありがとう"
    sleep(3)
    puts "\e[H\e[2J"
  end
  #マスターと会話・村について
  def mastertalk2
    sleep(0.9)
    puts "\nマスター：この村は神殿に一番近い村ってことで冒険者が沢山くる"
    sleep(0.9)
    puts "\n          その為に情報交換出来るウチの酒場やSHOPなど施設も様々だ"
    sleep(0.9)
    puts "\n          あんたも困ったことがあれば何でも聞いてくれ。"
    sleep(3)
    puts "\e[H\e[2J"

  end
  #マスターと会話・魔物について
  def mastertalk3
    sleep(0.9)
    puts "\nマスター：魔物は冒険者にとって一番厄介だ"
    sleep(0.9)
    puts "\n          逃げるに越した事はないが・・・倒すといいことがあるらしい"
    sleep(0.9)
    puts "\n          俺は戦いたいなんか思わないけどな。"
    sleep(3)
    puts "\e[H\e[2J"
  end
  ##マスターと会話・選択画面
  def mastertalk
    while true
     # 酒場の選択画面を表示する
     print "
     ／⌒~ヽノ~⌒＼
     `/ ⌒ヾヾｿﾉﾉ⌒ヽヽ
     ｜i /￣ヾノ￣ヽi |
     ｜|ｲ　　　　　|| |
     ﾉノ|ノ＼　／ヽ|ヽし
     彡ノ-･- ii -･-ヽミｲ
     ヽ(　ー || ー　)_ノ
     　｜　 ヽノ　 ｜｜
     　｜　　｜　　｜ﾉ
     　 ＼　ー―ヽ／
     　　 ＼＿＿／
     　　 ／|><|＼
     　　/ヽ|Ｖ|/ ヽ


     マスター：何が聞きたい？何でも答えるぜ。


     1.冒険について
     2.この村について
     3.魔物について
     9.他の人と話す

     番号を入力："


     num = gets.chomp
     case
     when '1' == num
       mastertalk1
       #冒険について
     when '2' == num
       mastertalk2
       #この村について
     when '3' == num
       mastertalk3
       #魔物について
     when '9' == num
       #選択肢に戻る
       break;
     else
     end
   end
 end

#剣士と会話
 def soldiertalk
   puts"
   ＿_/ﾚ/|／ﾚ／L＿
　＞ミ ミ ヾ 〃∠_
∠ ミ＿＿ﾐ＿〃＿ﾐ<
/ 彡｜　/ｿ　ﾚｿ ｜ﾐｱ
ﾚ 彡｜)　　　　(Ｙ
ヽ_ / 二＼) ノ二｜
（6ﾐ ヽ-ﾟ- ｲ-ﾟ-イ
　ﾋ|　　　 |　 /
　/ヽ　　　′ /
／ i ＼￣￣￣/
　 ヽ　＼＿／
　　　　 / ＼
＼＿　 厂　 ﾉヽ

"

   sleep(0.9)
   puts "\n剣士：君も冒険に出るのか？"
   puts""
   sleep(0.9)
   puts "\n#{$user_name}:きみは？"
   puts""
   sleep(0.9)
   puts "\n剣士：俺はこの村一番の大剣豪"
   puts""
   sleep(0.9)
   puts "\n#{$user_name}:良い剣だな。どこでに行けば手に入る？"
   puts""
   sleep(0.9)
   puts "\n剣士：この武器なら村のSHOPに行けば手に入るぞ"
   sleep(0.9)
   puts "\n      それにやくそうとか冒険には欠かせないアイテムもある"
   puts""
   sleep(0.9)
   puts "\n#{$user_name}:ありがとう。行ってみる"
   sleep(3)
   puts "\e[H\e[2J"
 end

 #酔っ払いと会話・お酒をあげる
 def drunktalk1
   sleep(0.9)
   puts "\n#{$user_name}:おじさん、ほどほどにね"
   sleep(0.9)
   puts "\n”お酒を渡した”"
   sleep(0.9)
   puts "\n酔っ払い：おお！お前はいいやつじゃ！お礼にいいことを教えてやろう"
   sleep(0.9)
   puts "\n          魔物と戦う度にお主は強くなる！それが経験値というものじゃ"
   sleep(0.9)
   puts""
   sleep(0.9)
   puts "\n#{$user_name}:おじさんありがとう"
   sleep(3)
   puts "\e[H\e[2J"
 end
 #酔っ払いと会話・お酒をあげない
 def drunktalk2
   sleep(0.9)
   puts "\n#{$user_name}:おじさん、アル中みたいだから酒はやめときな"
   puts""
   sleep(0.9)
   puts "\n酔っ払い：何だと！酒をくれぬ上に説教か！どっか行け！"
   puts""
   sleep(0.9)
   puts "\n#{$user_name}:(関わらない方がよさそうだ・・・)"
   puts""
   sleep(0.9)
   puts "\n酔っ払い：おぇぇぇぇ・・・酒じゃ、もっと酒を・・・"
   sleep(3)
   puts "\e[H\e[2J"
 end

 #酔っ払いと会話選択画面
 def drunktalk
   while true
     puts"
              ζ
     　　／￣￣￣￣＼
     　 /　　　　　　＼
     　////　　＼　／ ｜
     　L川　　(･)　(･)｜
     （6-―――◯⌒◯-｜ |
     　｜　　 ＿||||| |
     　 ＼ ／ ＼＿/　／
     　　 ＼______／

"

    sleep(0.9)
    puts"\n 酔っ払い：酒じゃ、酒をくれ！"
    sleep(0.9)
    puts"\n#{$user_name}:(この人は当てにならなそうだな)"
    sleep(0.9)
    puts "\e[H\e[2J"
    print"\n お酒をあげますか？
     1.あげる
     2.あげない

    番号を入力："

    num = gets.chomp
    case
    when '1' == num
      drunktalk1
      #上げる
      break;
    when '2' == num
      drunktalk2
      #あげない
      break;
    else
    end
  end
 end

 #酒場の選択画面を表示する
 def run
   while true

    print "
    マスター：いらっしゃい！ゆっくりしてきな。

    さて誰に話しかけようかな？

    1.マスターと話す
    2.剣士と話す
    3.酔っ払いと話す
    9.酒場を出る

    番号を入力："
    num = gets.chomp
    case
    when '1' == num
      mastertalk
      #マスターと話す
    when '2' == num
      soldiertalk
      #剣士と話す
    when '3' == num
      drunktalk
      #酔っ払いと話す
    when '9' == num
      #酒場を出る(MENUに戻る)
      puts "\nマスター：ありがとう。また来てくれよ。"
      break;
    else
    end
  end
end
end
