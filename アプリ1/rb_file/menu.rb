require './module.rb'

class Menu
  include CharacterDisplay
  def initialize( dbh )
    @dbh = dbh
  end

  def run
    while true
      # MENU画面を表示する
      print "
      1.冒険
      2.酒場
      3.自宅
      4.SHOP
      9.ゲーム開始画面に戻る。
      :"

      num = gets.chomp
      case
      when '1' == num
        #冒険
        adventure = Adventure.new(@dbh)
        adventure.run
      when '2' == num
        #酒場
        sakaba = Sakaba.new(@dbh)
        sakaba.run
      when '3' == num
        #自宅
        home = Home.new(@dbh)
        home.run
      when '4' == num
        #SHOP
        shop = Shop.new(@dbh)
        shop.run
      when '9' == num
      #MENUの終了
        puts "\nゲーム開始画面に戻る。"
        break;
      when '@' == num
        date_load
      else
      end # case
    end # while
  end # def
end # class
