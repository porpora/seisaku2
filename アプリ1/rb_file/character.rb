require 'rdbi'
require 'rdbi-driver-sqlite3'
require './adventure.rb'
require './badend.rb'
require './happyend.rb'
require './home.rb'
require './lastbattle'
require './menu.rb'
require './module.rb'
require './normalbattle.rb'
require './sakaba.rb'
require './shop.rb'

class Character
  def initialize( sqlite_name )
    @db_name = sqlite_name
    @dbh = RDBI.connect( :SQLite3, :database => "#{@db_name}" )

    sth = @dbh.execute( "select USER_NAME from character where id = 1;" )
    columns = sth.schema.columns.to_a
    character_info = sth.first
    character_info.each do |val|
      $user_name = val.to_s
    end

    sth = @dbh.execute( "select POWER from character where id = 1;" )
    columns = sth.schema.columns.to_a
    character_info = sth.first
    character_info.each do |val|
      $power = val.to_s
    end

    sth = @dbh.execute( "select DEFENSE from character where id = 1;" )
    columns = sth.schema.columns.to_a
    character_info = sth.first
    character_info.each do |val|
      $defense = val.to_s
    end
  end

  def date_load
    item_name = {'id' => "ID", 'USER_NAME' => "名前", 'SEX' => "性別", 'POWER' => "攻撃力", 'DEFENSE' => "防御力", 'LEVEL' => "レベル", 'MONEY' => "所持金", 'HP' => "体力", 'EXP_POINT' => "経験値" }
    sth = @dbh.execute(
    "select id, USER_NAME, SEX, POWER, DEFENSE, LEVEL, MONEY, HP, EXP_POINT from character where id = 1;"
    )
    columns = sth.schema.columns.to_a
    character_info = sth.first

    col_num = 0
    puts "-----------------------------------------"
    character_info.each do |val|
      puts "#{item_name[columns[col_num].name.to_s]}: #{val.to_s}"
      col_num += 1
    end # do
    puts "-----------------------------------------"
    puts "\n このキャラクターを使用しますか？　使用するなら[Y]をお願いします。"
    yesno = gets.chomp.upcase
    if /^Y$/ =~ yesno then
      menu = Menu.new(@dbh)
      menu.run
    else
      puts "使わないのかよ。。　はい。再起動ね"
      puts "※少々お待ちを"
    end # if
  end # date_load def

  def  update_equipment( name, price, power, defense, id )
   @dbh.execute("update equipment "\
                "set NAME = '#{name}', "\
                "PRICE = #{price}, "\
                "POWER = #{power}, "\
                "DEFENSE = #{defense} "\
                "where id = #{id};" )
  end

  def init_equipment
    ary = [["盾", "3000", "1000", "1000", "1",],
           ["盾", "4000", "1000", "1000", "2"],
           ["盾", "3000", "1000", "1000", "3"],
           ["盾", "3000", "1000", "1000", "4"],
           ["盾", "3000", "1000", "1000", "5"],
           ["盾", "3000", "1000", "1000", "6"]]
    ary.each do |row|
      update_equipment( row[0], row[1], row[2], row[3], row[4] )
    end
  end

  def date_new
    item_name = {'id' => "ID", 'USER_NAME' => "名前", 'SEX' => "性別", 'POWER' => "攻撃力", 'DEFENSE' => "防御力", 'LEVEL' => "レベル", 'MONEY' => "所持金", 'HP' => "体力", 'EXP_POINT' => "経験値" }
    sth = @dbh.execute("select "\
                       "id, "\
                       "USER_NAME, "\
                       "SEX, "\
                       "POWER, "\
                       "DEFENSE, "\
                       "LEVEL, "\
                       "MONEY, "\
                       "HP, "\
                       "EXP_POINT "\
                       "from character "\
                       "where id = 1;" )
    columns = sth.schema.columns.to_a
    character_info = sth.first

    col_num = 0
    puts "-----------------------------------------"
    character_info.each do |val|
      puts "#{item_name[columns[col_num].name.to_s]}: #{val.to_s}"
      col_num += 1
    end # do
    puts "-----------------------------------------"
    puts "\n このキャラクターデータは削除されます。よろしければ[Y]をお願いします。"

    yesno = gets.chomp.upcase
    if /^Y$/ =~ yesno
      puts  "\n名前を入力してください:"
       @user_name = gets.chomp
      puts  "\n性別を選んでください。(0:男、1:女)"
      @input_num = gets.chomp
      if @input_num == '0' then
        @sex = '男'
      elsif @input_num == '1' then
        @sex = '女'
      end # if
      puts "\n キャラクターを作成しました。"
    end # if
      @dbh.execute("update character "\
                   "set ID = 1, "\
                   "USER_NAME = '#{@user_name}', "\
                   "SEX = '#{@sex}', "\
                   "POWER = 1000, "\
                   "DEFENSE = 1000, "\
                   "LEVEL = 1, "\
                   "MONEY = 10000, "\
                   "HP = 1000, "\
                   "EXP_POINT = 0, "\
                   "MAX_EXP_POINT = 500, "\
                   "MAX_HP = 2000 "\
                   "where id = 1;" )

      init_equipment

      sth = @dbh.execute("select "\
                         "id, "\
                         "USER_NAME, "\
                         "SEX, "\
                         "POWER, "\
                         "DEFENSE, "\
                         "LEVEL, "\
                         "MONEY, "\
                         "HP, "\
                         "EXP_POINT "\
                         "from character "\
                         "where id = 1;" )
      columns = sth.schema.columns.to_a
      character_info = sth.first

      col_num = 0
        puts "\n-----------------------------------------"
      character_info.each do |val|
        puts "#{item_name[columns[col_num].name.to_s]}: #{val.to_s}"
        col_num += 1
      end # do
        puts "-----------------------------------------"
        puts " メニュー画面に戻ります"
        sleep(3)
  end # date_new

  def run
    while true
      puts "\nGAME起動中です。少々お待ちください。"
      puts "3"
      sleep(1)
      puts "2"
      sleep(1)
      puts "1"
      sleep(1)
      print "
       ＿へーへ＿
   　 /　　　　　ヽ
   　[二二二二二二]
   ／＿＿＿＿＿＿＿＼
   （｣ ／o|∩|o＼ L)
   (/〈＿,(＿)､＿〉ﾍ)
   ｜ヽ工工工工工７｜
   `／￣＼エエエｴﾉ /
   (　　 ｜＿＿＿／
   |＼＿／ヽ　∧∧∧∧
   |　＿_(￣|<ドーン!!>
   |　＿_)_ノ ＶＶＶＶ
   ヽ＿_)ノ

"

      print"
      0.ロード
      1.新規作成
      9.アプリ終了
      番号を選んでください(0.1.9) :"

      num = gets.chomp
      case
      when '0' == num
        date_load
      when '1' == num
        date_new
      when '9' == num
        puts "アプリを終了します"
        @dbh.disconnect
        #game_finish
      break;
      else
      end # case
    end # while
  end # run def
end # class

character = Character.new("seisaku.db")
character.run
