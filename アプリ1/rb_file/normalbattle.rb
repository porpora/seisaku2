class Battle
  def initialize( dbh )
    @dbh = dbh
  end

  #エネミーのHPを初期化する
  def enemy_hpreset
    hp_reset = @dbh.execute(
     "update enemy set HP = (select MAX_HP from enemy where id = 1) where id = 1;")
  end #def enemy_hpreset

  #プレイヤーの攻撃処理
  #キャラクターの攻撃力を取り出す
  def character_atack
    character_power = @dbh.execute(
      "select POWER from character where id = 1;")
    character_power.each do |val|
      @character_power = val
    end #character_power

  #エネミーのHPを取り出す
    enemy_hp = @dbh.execute(
      "select HP from enemy where id = 1;")
    enemy_hp.each do |val|
      @enemy_hp = val
      #puts "敵の残りHPは#{@enemy_hp[0]}"
    end # enemy_hp

  #エネミーの防御力を取り出す
    enemy_defense = @dbh.execute(
      "select DEFENSE from enemy where id = 1;")
    enemy_defense.each do |val|
      @enemy_defense = val
    end #enemy_defense

  #キャラクターの攻撃力とエネミーの防御力を引き算し、ダメージ値を作る

    @damege = @character_power[0] - @enemy_defense[0]
    p "#{@damege} のダメージを与えた！"

  #敵のHPからダメージ値を引き算する
      enemy_hp_record = @dbh.execute(
        "UPDATE enemy SET HP = #{@enemy_hp[0] - @damege}")

  #勝利した場合の条件分岐
        if @enemy_hp[0] < 0
          exp_and_money

          #ここにダンジョンのメソッドを追加する

        end # if @enemy_hp
  end # def character_atack


  #エネミーの攻撃処理
  def enemy_atack
  #エネミーの攻撃力を取り出す
    enemy_power = @dbh.execute(
      "select POWER from enemy where id = 1;")
    enemy_power.each do |val|
      @enemy_power = val
    end

#キャラクターのHPを取り出す
    character_hp = @dbh.execute(
       "select HP from character where id = 1;")
    character_hp.each do |val|
        @character_hp = val
    end

#キャラクターの防御力を取り出す
    character_defense = @dbh.execute(
        "select DEFENSE from character where id = 1;")
    character_defense.each do |val|
        @character_defense = val
    end

#エネミーの攻撃力とキャラクターの防御力を引き算し、ダメージ値を作る
    @defense_damege = @enemy_power[0] - @character_defense[0]
    if @defense_damege < 0
      p "0ダメージを受けた！"
            else
      p "#{@defense_damege} のダメージを受けた"

    end

  #キャラクターのHPからダメージ値を引き算する
    character_hp_record = @dbh.execute(
      "UPDATE character SET HP = #{@character_hp[0] - @defense_damege}")


  #キャラクターの経験値とお金の値を取り出す
  def exp_and_money
    oppai =  @dbh.execute(
      "select EXP_POINT, MONEY from character where id = 1;")
    oppai.each do |val|
      @character_exp_and_maney = val
      p "#{@character_exp_and_maney}をかくとく！"
    end

  #エネミーの経験値とお金の値を取り出す
    enemy_exp_and_money = @dbh.execute(
      "select EXPERIENCE_POINT, MONEY from enemy where id = 1;")
    enemy_exp_and_money.each do |val|
      @enemy_exp_and_money = val
      p "#{@character_exp_and_maney}をかくとく!"
    end

    #キャラクターの経験値・マネーと、エネミーの経験値・マネーを合算する
    total_exp = @dbh.execute(
    "UPDATE character SET EXP_POINT = #{@enemy_exp_and_money[0] + @character_exp_and_maney[0]},
    MONEY = #{@enemy_exp_and_money[1] + @character_exp_and_maney[1]}")
  end # def exp_and_money


  #レベルアップ
  def level_up
    if @enemy_hp == 0
      #キャラクターのLEVELを取り出す
      character_level = @dbh.execute(
        "select LEVEL from character where id = 1;")
      character_level.each do |val|
        @character_level = val
      end

      #キャラクターのHPを取り出す
      character_max_hp = @dbh.execute(
        "select MAX_HP from character where id = 1;")
      character_max_hp.each do |val|
        @character_max_hp = val
      end

      #キャラクターの攻撃力を取り出す
      character_power = @dbh.execute(
        "select POWER from character where id = 1;")
      character_power.each do |val|
        @character_power = val
      end

      #キャラクターの防御力を取り出す
      character_defense = @dbh.execute(
         "select DEFENSE from character where id = 1;")
      character_defense.each do |val|
        @character_defense = val
      end

      #主人公の経験値を取り出す
      character_exp_point = @dbh.execute(
        "select EXPERIENCE_POINT from character where id =1")
      character_exp_point.each do |val|
        @character_exp_point = val
      end

      #エネミーから経験値を取り出す
      enemy_experience_point = @dbh.execute(
        "select EXPERIENCE_POINT from enemy where id =1")
      enemy_experience_point.each do |val|
        @enemy_experience_point = val
      end


        #レベルアップの計算式
        @character_level_up =
        (@character_exp_point + @enemy_exp_point) > @character_max_exp_point

        #レベルの更新
        if @character_level_up
          @character_level += 1
          @character_max_hp += 300
          @character_power +=300
          @character_defense +=300
          @character_max_exp_point +=500

          @dbh.execute(
            "UPDATE character SET
            LEVEL = #{@character_level} ,
            HP = #{@character_max_hp}
            MAX_HP = #{@character_max_hp} ,
            POWER = #{@character_power} ,
            DEFENSE = #{@character_defense} ,
            MAX_EXP_POINT = #{@character_max_exp_point}

            where id =1 ")
        end
      end #if
    end #end

  def run
    puts "モンスターが現れた！"
     enemy_hpreset
    loop do
      puts "1:こうげき"
      puts "2:にげる"

      @input = gets.chomp
      case @input
      when "1"
        character_atack
        enemy_atack

      when "2"
        break
      end
    end
  end
end
