class Shop
  def initialize( dbh )
    @dbh = dbh
  end

    def character_update
 # 装備品の攻撃力、防御力を取得。 IDはinputで取得
      chr = @dbh.execute(
         "select POWER, DEFENSE from equipment where id = #{@input};")
      chr.each do |val|
         @equipment_status = val
    end

 # キャラクターの攻撃力、防御力を取得
      eqi = @dbh.execute(
         "select POWER, DEFENSE from character where id = 1;")
      eqi.each do |val|
         @character_status = val
    end

 # キャラクターの攻撃力と防御力を合算
      total = @dbh.execute(
         "UPDATE character SET POWER = #{@character_status[0] + @equipment_status[0]},
         DEFENSE = #{@character_status[1] + @equipment_status[1]}")

         p @input
         p @character_status[0]
         p @character_status[0] + @equipment_status[0]
         p @character_status[1]
         p @character_status[1] + 1000
         p @character_status[0] + @equipment_status[0]

 # 一度購入したアイテムの値を0にする
      second_buy = @dbh.execute(
        "UPDATE equipment SET POWER = 0, DEFENSE = 0 where id = #{@input};")
   end #character_update

   def buy
 # 主人公のマネーを取り出す
       character_money = @dbh.execute(
          "select MONEY from character where id = 1;")
       character_money.each do |val|
          @character_money = val

       end
 # 購入したアイテムのマネーを取り出す
       item_price = @dbh.execute(
          "select PRICE from equipment where id = #{@input};")
       item_price.each do |val|
          @item_price = val
       end

 # 購入したアイテムの名前を取り出す
         item_name = @dbh.execute(
            "select NAME from equipment where id = #{@input};")
         item_name.each do |val|
            @item_name = val
         end

 # 購入できなかった場合の処理
       if @character_money[0] < @item_price[0]
           puts "お金がありません。"
           else

 # 購入できた場合、主人公の所持金からアイテムの購入代を引く
       buy_maney = @dbh.execute(
         "UPDATE character SET MONEY = #{@character_money[0] - @item_price[0]}")
         puts "#{ @item_name[0]}を購入しました"
      character_update
       end
    end #buyメソッド

  # 標準出力のクリアを行う
  def clear
    puts "\e[H\e[2J"
    puts "\e[H\e[2J"
  end

  # 店内メニュー
  def run
    clear
    loop do
      puts " いらっしゃい。何を買うんだ?"
      puts " "
      puts "1: 鋼の剣     3000G"
      puts "2: 鋼の盾     3000G"
      puts "3: 鋼の鎧     3000G"
      puts "4: 腕輪       3000G"
      puts "5: 指輪       3000G"
      puts "6: ピアス     3000G"
      puts "9: 帰る"

      @input = gets.chomp
      case @input
      when "1"
        clear
        buy

      when "2"
        clear
        buy

      when "3"
        clear
        buy

      when "4"
        clear
        buy

      when "5"
        clear
        buy

      when "6"
        clear
        buy

      when "9"
        clear
        break
      else
        clear
        puts "そんな商品はない"
        next
      end
    end
  end
end
